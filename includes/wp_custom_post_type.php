<?php

if (!defined('ABSPATH')) {
	die('Invalid request.');
}

/*
function taurusfood_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'taurusfood' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'taurusfood' ),
		'menu_name'             => __( 'Clientes', 'taurusfood' ),
		'name_admin_bar'        => __( 'Clientes', 'taurusfood' ),
		'archives'              => __( 'Archivo de Clientes', 'taurusfood' ),
		'attributes'            => __( 'Atributos de Cliente', 'taurusfood' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'taurusfood' ),
		'all_items'             => __( 'Todos los Clientes', 'taurusfood' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'taurusfood' ),
		'add_new'               => __( 'Agregar Nuevo', 'taurusfood' ),
		'new_item'              => __( 'Nuevo Cliente', 'taurusfood' ),
		'edit_item'             => __( 'Editar Cliente', 'taurusfood' ),
		'update_item'           => __( 'Actualizar Cliente', 'taurusfood' ),
		'view_item'             => __( 'Ver Cliente', 'taurusfood' ),
		'view_items'            => __( 'Ver Clientes', 'taurusfood' ),
		'search_items'          => __( 'Buscar Cliente', 'taurusfood' ),
		'not_found'             => __( 'No hay resultados', 'taurusfood' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'taurusfood' ),
		'featured_image'        => __( 'Imagen del Cliente', 'taurusfood' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'taurusfood' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'taurusfood' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'taurusfood' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'taurusfood' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'taurusfood' ),
		'items_list'            => __( 'Listado de Clientes', 'taurusfood' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'taurusfood' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'taurusfood' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'taurusfood' ),
		'description'           => __( 'Portafolio de Clientes', 'taurusfood' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'taurusfood_custom_post_type', 0 );
*/
