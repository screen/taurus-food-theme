![alt tag](images/repo-logo.jpg)

# Taurus Food - Wordpress Custom Theme #

* Version: 1.0.0
* Design: [Robert Ochoa](http://www.robertochoaweb.com/?utm_source=github_link&utm_medium=link&utm_content=taurusfood)
* Development: [Robert Ochoa](http://www.robertochoaweb.com/?utm_source=github_link&utm_medium=link&utm_content=taurusfood)

Tema diseñado por [Robert Ochoa](http://www.robertochoaweb.com/?utm_source=github_link&utm_medium=link&utm_content=taurusfood) para Taurus Food.
Este tema custom fue construido en su totalidad, pasando por su etapa de Wireframing, rearmado, version anterior e implementación en hosting externo.

### Componentes Principales ###

* Twitter Bootstrap 5.0.2

### Funciones Incluídas ###

* Custom Post Type.
* Custom Taxonomies.
* Bootstrap Ready: Wordpress Menu Structure.
* Custom Metabox.

### Plugins Requeridos ###

* CMB2

### Instrucciones de Instalación ###

1. Instalar los plugins requeridos.

2. Activar los plugins requeridos.

3. Instalar el tema via FTP o por el instalador de themes de Wordpress via zip

### Contacto ###

Soporte Oficial para este tema:

Repo Owner: [Robert Ochoa](http://www.robertochoaweb.com/?utm_source=github_link&utm_medium=link&utm_content=taurusfood)

Main Developer: [Robert Ochoa](http://www.robertochoaweb.com/?utm_source=github_link&utm_medium=link&utm_content=taurusfood)
